json.array! @friends do |r|
  json.screenname r['screen_name']
  json.profile_image_url r['profile_image_url']
end
