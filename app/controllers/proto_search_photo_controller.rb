# coding: utf-8
class ProtoSearchPhotoController < ApplicationController
  def show
    text = "祇園"
    @res = search(text)
  end
end
private
def search(text)
  res = RestClient.get 'https://api.flickr.com/services/rest', {:params => {:method => 'flickr.photos.search', :api_key => ENV['FL_CONS_KEY'], :text => text, :format => 'json', :sort => 'relevance', :per_page => '10'}}
  res.slice!(0,14)
  res.slice!(-1,1)
  JSON.parse(res)
end
