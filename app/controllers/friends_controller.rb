# coding: utf-8
class FriendsController < ApplicationController
  before_action :authenticate_user!
  def show
    ids = current_user.client.friend_ids(current_user.username)
    @friends = current_user.client.users(ids.take(21))
  end
  def next_users
    # TODO:フフォロワー数を取得して、その件数以上取得しないように
    @page = params["page"]
    if @page.is_a?(Integer)
      ids =  current_user.client.friend_ids(current_user.username)["ids"]
      @friends = current_user.client.users(ids[(20*page)..(20*(page+1))])
    end
  end
end
