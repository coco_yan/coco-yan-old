# coding: utf-8
require 'twitter'

client = Twitter::REST::Client.new do |config|
  config.consumer_key        = ENV[TW_CONS_KEY]
  config.consumer_secret     = ENV[TW_CONS_SECRET_KEY]
end

query = ""
